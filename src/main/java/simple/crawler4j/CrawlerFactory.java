package simple.crawler4j;

/**
 * Because we have multiple threads - we need something to construct
 * new crawlers.
 *
 * Also, crawlers might and would be re-created because of memory leaks in
 * HtmlUnit (JS engine is not perfect)
 *
 * @param <T> data that you passes as a result from each page
 */
public interface CrawlerFactory<T>
{
	Crawler<T> createInstance(CrawlerConfig<T> config);
}
