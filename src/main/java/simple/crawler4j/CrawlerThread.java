package simple.crawler4j;

import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.istack.internal.NotNull;

import simple.crawler4j.url.WebURL;

/**
 * Created by mkaraman on 2/7/17.
 */
public class CrawlerThread<T> implements Runnable
{
	private final static Logger LOGGER = LoggerFactory.getLogger(CrawlerThread.class);

	private final Crawler<T> crawler;
	private long withoutJob = 0L;
	private long lastFetch = 0L;
	private final Queue<WebURL> needToVisitQueue;
	private final CrawlerConfig config;
	private final Set<WebURL> visitedPages;

	public CrawlerThread(@NotNull Crawler<T> crawler, @NotNull Queue<WebURL> needToVisitQueue, @NotNull CrawlerConfig config,
		@NotNull Set<WebURL> visitedPages)
	{
		this.crawler = crawler;
		this.needToVisitQueue = needToVisitQueue;
		this.config = config;
		this.visitedPages = visitedPages;
	}

	@Override
	public void run()
	{
		try
		{
			process();
		}
		catch (InterruptedException e)
		{
			LOGGER.error("Failed to wait during delay, stop controller", e);
		}
	}

	private void process() throws InterruptedException
	{
		while (true)
		{
			sleepIfNeedsToBePolite();
			WebURL currentUrl = needToVisitQueue.poll();

			if (shouldStopWithTimeout(currentUrl, config.getTimeoutMilliseconds()))
			{
				break;
			}
			if (currentUrl == null)
			{
				continue;
			}
			visitedPages.add(currentUrl);
			lastFetch = System.currentTimeMillis();
			PageResults<T> results = crawler.fetchAndProcess(currentUrl.getURL());
			processPageLinks(currentUrl, results);
			withoutJob = 0L;
		}
	}

	private void processPageLinks(WebURL currentUrl, PageResults<T> results)
	{
		if (results == null)
		{
			LOGGER.info("No results from crawler. Error during quering?");
			return;
		}
		results.getUrls().stream().forEach(webURL -> webURL.setDepth((short) (currentUrl.getDepth() + 1)));
		needToVisitQueue.addAll(results.getUrls().stream().filter(url -> !visitedPages.contains(url.getURL())).collect(Collectors.toList()));
	}

	private boolean shouldStopWithTimeout(WebURL currentUrl, int timeoutMilliseconds) throws InterruptedException
	{
		if (currentUrl == null)
		{
			if (withoutJob == 0L)
			{
				withoutJob = System.currentTimeMillis();
			}
			long hasNoWorkForMS = System.currentTimeMillis() - withoutJob;
			if (hasNoWorkForMS < timeoutMilliseconds * 20)
			{
				LOGGER.info("Has no work for " + hasNoWorkForMS + " ms. I'm going to sleep for " + timeoutMilliseconds + " ms");
				TimeUnit.MILLISECONDS.sleep(timeoutMilliseconds);
				return false;
			}
			LOGGER.info("Has no work for " + hasNoWorkForMS + " ms. STOPPED");
			crawler.close();
			return true;
		}
		return false;
	}

	private void sleepIfNeedsToBePolite() throws InterruptedException
	{
		long delayBetweenCalls = System.currentTimeMillis() - lastFetch;
		if (delayBetweenCalls < config.getPolitenessDelay())
		{
			TimeUnit.MILLISECONDS.sleep(delayBetweenCalls);
		}
	}
}
