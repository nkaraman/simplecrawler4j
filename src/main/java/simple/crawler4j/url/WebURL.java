/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simple.crawler4j.url;

import java.io.Serializable;

import javax.annotation.Nullable;

import com.sun.istack.internal.NotNull;

/**
 * @author Yasser Ganjisaffar
 */

public class WebURL implements Serializable
{

	private static final long serialVersionUID = 1L;

	private String url;
	private short depth;
	private String domain;
	private String subDomain;
	private String path;

	public WebURL(@NotNull String url)
	{
		this(url, null);
	}

	public WebURL(@NotNull String url, @Nullable String context)
	{
		if (url == null)
		{
			throw new IllegalArgumentException("Url can not be null");
		}
		setURL(url, context);
	}

	/**
	 * @return Url string
	 */
	public String getURL()
	{
		return url;
	}

	private void setURL(String url, String context)
	{
		this.url = URLCanonicalizer.getCanonicalURL(url, context);
		if (url.isEmpty())
		{
			this.url = null;
			return;
		}
		final int domainStartIndex = getDomainStart(url);
		final int domainEndIndex = getDomainEnd(url, domainStartIndex);

		domain = url.substring(domainStartIndex, domainEndIndex);
		subDomain = "";
		String[] parts = domain.split("\\.");
		if (parts.length > 2)
		{
			domain = parts[parts.length - 2] + "." + parts[parts.length - 1];
			int limit = 2;
			if (TLDList.getInstance().contains(domain))
			{
				domain = parts[parts.length - 3] + "." + domain;
				limit = 3;
			}
			for (int i = 0; i < (parts.length - limit); i++)
			{
				if (!subDomain.isEmpty())
				{
					subDomain += ".";
				}
				subDomain += parts[i];
			}
		}
		path = url.substring(domainEndIndex);
		int pathEndIdx = path.indexOf('?');
		if (pathEndIdx >= 0)
		{
			path = path.substring(0, pathEndIdx);
		}
	}

	private int getDomainEnd(String url, int domainStartIdx)
	{
		int domainEndIdx = url.indexOf('/', domainStartIdx);
		domainEndIdx = (domainEndIdx > domainStartIdx) ? domainEndIdx : url.length();
		return domainEndIdx;
	}

	private int getDomainStart(String url)
	{
		int slashesPosition = url.indexOf("//");
		return (slashesPosition == -1) ? 0 : slashesPosition + 2;
	}

	/**
	 * @return crawl depth at which this Url is first observed. Seed Urls are at
	 * depth 0. Urls that are extracted from seed Urls are at depth 1,
	 * etc.
	 */
	public short getDepth()
	{
		return depth;
	}

	public void setDepth(short depth)
	{
		this.depth = depth;
	}

	/**
	 * @return domain of this Url. For 'http://www.example.com/sample.htm',
	 * domain will be 'example.com'
	 */
	public String getDomain()
	{
		return domain;
	}

	public String getSubDomain()
	{
		return subDomain;
	}

	/**
	 * @return path of this Url. For 'http://www.example.com/sample.htm', domain
	 * will be 'sample.htm'
	 */
	public String getPath()
	{
		return path;
	}

	@Override
	public int hashCode()
	{
		return url.hashCode();
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if ((o == null) || (getClass() != o.getClass()))
		{
			return false;
		}

		WebURL otherUrl = (WebURL) o;
		return (url != null) && url.equals(otherUrl.getURL());

	}

	@Override
	public String toString()
	{
		return url;
	}
}