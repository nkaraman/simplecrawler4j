package simple.crawler4j.url;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is a singleton which obtains a list of TLDs (from online or a local file) in order to compare against
 * those TLDs
 * Implementation is wrong :)
 * Need to spend sometime to review this Opensource
 */
public class TLDList
{

	private static final String TLD_NAMES_ONLINE_URL = "https://publicsuffix.org/list/effective_tld_names.dat";
	public static final String TLD_NAMES_TXT_FILENAME = "tld-names.txt";
	private static final Logger logger = LoggerFactory.getLogger(TLDList.class);

	private static boolean onlineUpdate = false;
	private final Set<String> tldSet = new HashSet<>(10000);

	private static final TLDList instance = new TLDList(); // Singleton

	private TLDList()
	{

		File f = new File(TLD_NAMES_TXT_FILENAME);
		if (onlineUpdate || !f.exists())
		{
			if (!downloadTheFile())
			{
				throw new RuntimeException("Could not download TLD file");
			}
		}

		if (f.exists())
		{
			logger.debug("Fetching the list from a local file {}", TLD_NAMES_TXT_FILENAME);
			try (InputStream tldFile = new FileInputStream(f))
			{
				int n = readStream(tldFile);
				logger.info("Obtained {} TLD from local file {}", n, TLD_NAMES_TXT_FILENAME);
				return;
			}
			catch (IOException e)
			{
				logger.error("Couldn't read the TLD list from local file", e);
			}
		}

		throw new RuntimeException("Absolut path is: " + f.getAbsolutePath());
	}

	private boolean downloadTheFile()
	{
		URL url;
		try
		{
			url = new URL(TLD_NAMES_ONLINE_URL);
		}
		catch (MalformedURLException e)
		{
			logger.error("Invalid URL: {}", TLD_NAMES_ONLINE_URL);
			throw new RuntimeException(e);
		}

		try (InputStream stream = url.openStream())
		{
			logger.debug("Fetching the most updated TLD list online");
			writeStreamToFile(stream, new File(TLD_NAMES_TXT_FILENAME));
			return true;
		}
		catch (Exception e)
		{
			logger.error("Couldn't fetch the online list of TLDs from: {}", TLD_NAMES_ONLINE_URL, e);
		}
		return false;
	}

	private void writeStreamToFile(InputStream inputStream, File file)
	{
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			BufferedWriter writer = new BufferedWriter(new FileWriter(file))
		)
		{
			String line;
			while ((line = reader.readLine()) != null)
			{
				line = line.trim();
				if (line.isEmpty() || line.startsWith("//"))
				{
					continue;
				}
				writer.write(line + "\n");
			}
		}
		catch (IOException e)
		{
			logger.warn("Error while reading TLD-list: {}", e.getMessage());
		}
	}

	private int readStream(InputStream stream)
	{
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream)))
		{
			String line;
			while ((line = reader.readLine()) != null)
			{
				line = line.trim();
				if (line.isEmpty() || line.startsWith("//"))
				{
					continue;
				}
				tldSet.add(line);
			}
		}
		catch (IOException e)
		{
			logger.warn("Error while reading TLD-list: {}", e.getMessage());
		}
		return tldSet.size();
	}

	public static TLDList getInstance()
	{
		return instance;
	}

	/**
	 * If {@code online} is set to true, the list of TLD files will be downloaded and refreshed, otherwise the one
	 * cached in src/main/resources/tld-names.txt will be used.
	 */
	public static void setUseOnline(boolean online)
	{
		onlineUpdate = online;
	}

	public boolean contains(String str)
	{
		return tldSet.contains(str);
	}
}
