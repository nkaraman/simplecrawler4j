package simple.crawler4j;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptJobManager;
import com.sun.istack.internal.NotNull;

import simple.crawler4j.url.WebURL;
import simple.crawler4j.validation.Validator;

/**
 * Worker that will be spawned for getting information from the Web
 *
 * @param <T> - type of the data that crawler returns after work
 * @author mkaraman
 */
public abstract class Crawler<T>
{
	final static Logger LOGGER = LoggerFactory.getLogger(Crawler.class);

	private CrawlerConfig<T> config;
	protected WebClient webClient;
	protected int visitedPages = 0;
	private WebClientFactory webClientFactory;

	public Crawler(@NotNull CrawlerConfig<T> config, @NotNull WebClientFactory webClientFactory)
	{
		this.config = config;
		this.webClientFactory = webClientFactory;
		init(config);
	}

	/**
	 * Useful for adding/changing some crawler specific settings
	 *
	 * @param webClient
	 */
	public void onWebClientConfig(WebClient webClient)
	{

	}

	/**
	 * You need to define how to authenticate here
	 *
	 * @param webClient
	 */
	public void onAuthentication(WebClient webClient)
	{

	}

	public PageResults<T> fetchAndProcess(@NotNull String url)
	{
		restartCrawlerToPreventJSMemoryLeaks();
		try
		{
			Page newPage = webClient.getPage(url);
			if (!(newPage instanceof HtmlPage))
			{
				LOGGER.info("Some page that is not HTML:" + newPage.getClass().getName());
				return null;
			}
			HtmlPage page = (HtmlPage) newPage;
			waitForJavaScriptToLoad(page);
			T results = onVisit(page);
			PageResults<T> pageResults = new PageResults<>(page, results);
			applyUrlsToResults(pageResults, url);

			if (pageResults.getResults() != null)
			{
				LOGGER.info("Going to save data " + url);
				String errorMessage = validate(pageResults.getResults());
				if (errorMessage != null)
				{
					LOGGER.warn("Result is not valid:" + errorMessage);
					return pageResults;
				}
				onSave(pageResults);
			}
			return pageResults;
		}
		catch (Exception e)
		{
			init(config);
			LOGGER.error("Error during fetching page ", e);
			return null;
		}
	}

	private void restartCrawlerToPreventJSMemoryLeaks()
	{
		if (config.getRestartWebClientEveryNPages() > 0 && visitedPages % config.getRestartWebClientEveryNPages() == 0)
		{
			init(config);
		}
	}

	private void waitForJavaScriptToLoad(HtmlPage page)
	{
		Optional<PatternTimeout> patternOptional = config.getPatterTimeouts().stream()
			.filter(patternTimeout -> page.getUrl().toString().contains(patternTimeout.pattern)).findFirst();
		if (!patternOptional.isPresent())
		{
			return;
		}

		PatternTimeout patternTimeout = patternOptional.get();

		JavaScriptJobManager manager = page.getEnclosingWindow().getJobManager();
		long startedWaiting = System.currentTimeMillis();
		while (manager.getJobCount() > 0 && (startedWaiting + patternTimeout.timeout) >= System.currentTimeMillis())
		{
			try
			{
				if (patternTimeout.timeout > 50000L)
				{
					LOGGER.info("JS still loading for page " + page.getUrl().toString());
				}
				Thread.sleep(patternTimeout.timeout / 10);

			}
			catch (InterruptedException e)
			{
				LOGGER.error("Failed to wait finish of JS load for page " + page.getUrl().toString(), e);
			}
		}
	}

	public abstract boolean shouldBeVisited(String url);

	/**
	 * After visiting page we need to have a real object that we would save to Database or other
	 * storages We don't save page to disk or something.
	 *
	 * @param page
	 * @return
	 */
	public abstract T onVisit(HtmlPage page);

	/**
	 * The way how we parse page and receive further links that we need to visit
	 *
	 * @param page
	 * @param context
	 * @return
	 */
	public Set<WebURL> onNextPagesSearch(HtmlPage page, String context)
	{
		return page.getAnchors().stream().map(HtmlAnchor::getHrefAttribute).map(url -> new WebURL(url, context))
			.filter(webURL -> webURL.getURL() != null).filter(webURL -> this.shouldBeVisited(webURL.getURL())).collect(Collectors.toSet());
	}

	final String validate(T t)
	{
		if (config.getValidator() == null)
		{
			return null;
		}
		return validate(t, config.getValidator());
	}

	private String validate(T t, Validator<T> validator)
	{
		String result = validator.validate(t);
		if (StringUtils.isNotEmpty(result))
		{
			return result;
		}
		for (Validator<T> entryValidator : validator.getValidationChain())
		{
			result = validate(t, entryValidator);
			if (StringUtils.isNotEmpty(result))
			{
				return result;
			}
		}
		return null;
	}

	/**
	 * If it's required to save data somewhere you should modify this method
	 *
	 * @param pageResults
	 */
	public void onSave(PageResults<T> pageResults)
	{
		if (config.getCrawledDataSaver() != null)
		{
			config.getCrawledDataSaver().save(pageResults.getResults());
		}
	}

	public void close()
	{
		webClient.close();
	}

	protected void init(CrawlerConfig<T> config)
	{
		if (webClient != null)
		{
			webClient.close();
		}

		webClient = webClientFactory.create(config);

		onWebClientConfig(webClient);
		onAuthentication(webClient);
	}

	private void applyUrlsToResults(PageResults<T> pageResults, String context)
	{
		Set<WebURL> nextPages = onNextPagesSearch(pageResults.getPage(), context);
		StringBuilder sb = new StringBuilder("Found links for crawling:\n");
		for (WebURL url : nextPages)
		{
			sb.append("\n\t\t" + url.toString());
		}
		LOGGER.info(sb.toString());
		pageResults.setUrls(nextPages);
	}

}
