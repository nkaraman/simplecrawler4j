package simple.crawler4j;

import com.gargoylesoftware.htmlunit.WebClient;

/**
 * Created by mkaraman on 2/27/17.
 */
public class WebClientFactory
{
	public WebClient create(CrawlerConfig config)
	{

		WebClient webClient = new WebClient();
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		webClient.getOptions().setCssEnabled(false);
		webClient.getOptions().setTimeout(config.getTimeoutMilliseconds());
		webClient.getOptions().setJavaScriptEnabled(true);

		return webClient;
	}
}
