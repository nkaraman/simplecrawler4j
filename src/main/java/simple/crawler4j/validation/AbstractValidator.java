package simple.crawler4j.validation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public abstract class AbstractValidator<E> implements Validator<E>{

  protected List<Validator<E>> validators = new ArrayList<>();
  
  /**
   * @throws NullPointerException if tries to add null
   */
  @Override
  public void addValidatorToTheChain(Validator<E> validator) {
    if (validator == null) {
      throw new NullPointerException();
    }
    validators.add(validator);
  }

  @Override
  public Collection<Validator<E>> getValidationChain() {
    return Collections.unmodifiableCollection(validators);
  }

}
