package simple.crawler4j.validation;

import java.util.Collection;

/**
 * Helps to create a chain of validators to validate data that comes from crawler
 *
 * @author mkaraman
 */
public interface Validator<E>
{
	/**
	 * @param e item to validate
	 * @return described message why item is not valid or null if everything is fine
	 */
	String validate(E e);

	/**
	 * Makes a new dependency that would be validated after this when ever {@link #validate(Object)} called
	 *
	 * @param validator
	 */
	void addValidatorToTheChain(Validator<E> validator);

	Collection<Validator<E>> getValidationChain();
}
