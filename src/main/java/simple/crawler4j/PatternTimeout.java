package simple.crawler4j;

import com.sun.istack.internal.NotNull;

/**
 * We use HtmlUnit only for loading JS. Still during processing you would receive page with not all JS loaded.
 * You can wait until page is finished (by waiting for some HTML construction to appear on visit)
 * or you can give page some time to load.
 * However, you can not wait forever. A lot of pages newer complete JS on the page. Because of tracking or some
 * other HTML manipulations (timers).
 * Also, because of that you can not just put a huge timeout on every page.
 * Currently, we don't need some complex logic with regular expressions here.
 * That is why it is just a String.contains.
 *
 * The order in list where you add patterns matters!
 *
 * @author mkaraman
 */
public class PatternTimeout
{
	public final String pattern;
	public final long timeout;

	public PatternTimeout(@NotNull String pattern, long timeout)
	{
		super();
		if (pattern == null)
		{
			throw new NullPointerException();
		}
		this.pattern = pattern;
		this.timeout = timeout;
	}

}
