package simple.crawler4j;

import java.util.List;

/**
 * The way to save data to any kind of storage.
 * Just implement this interface and give it inside of {@link CrawlerConfig#setCrawledDataSaver(CrawledDataSaver)}
 *
 * @param <T> data type
 */
public interface CrawledDataSaver<T> {
	/**
	 * It would call it every time when page fetched and parsed.
	 * Please do batching inside of this method if do need it
	 *
	 * @param t data type
	 */
    void save(T t);

	/**
	 * The way to re-fetch data from urls. Use it when you need to renew the data.
	 * Or when you have a big list of urls that you need just to fetch.
	 *
	 * @return list of urls.
	 */
	List<String> preloadUrls();
}
