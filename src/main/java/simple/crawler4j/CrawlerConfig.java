package simple.crawler4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import simple.crawler4j.validation.Validator;

public class CrawlerConfig<T>
{
	private long politenessDelay = 1000L;
	private int timeoutMilliseconds = 3000;
	private final List<String> urlsToStart = new ArrayList<String>();
	private int crawlers = 1;
	private CrawledDataSaver<T> crawledDataSaver;
	private Validator<T> validator;
	private final List<PatternTimeout> patterTimeouts = new ArrayList<>();
	private int restartWebClientEveryNPages = 10;

	public int getCrawlers()
	{
		return crawlers;
	}

	public List<String> getUrlsToStart()
	{
		return Collections.unmodifiableList(urlsToStart);
	}

	public long getPolitenessDelay()
	{
		return politenessDelay;
	}

	public int getTimeoutMilliseconds()
	{
		return timeoutMilliseconds;
	}

	public CrawledDataSaver<T> getCrawledDataSaver()
	{
		return crawledDataSaver;
	}

	public Validator<T> getValidator()
	{
		return validator;
	}

	public List<PatternTimeout> getPatterTimeouts()
	{
		return patterTimeouts;
	}

	public CrawlerConfig<T> setPolitenessDelay(long politenessDelay)
	{
		this.politenessDelay = politenessDelay;
		return this;
	}

	public CrawlerConfig<T> setTimeoutMilliseconds(int timeout)
	{
		this.timeoutMilliseconds = timeout;
		return this;
	}

	public CrawlerConfig<T> setCrawledDataSaver(CrawledDataSaver<T> crawlerSaveDao)
	{
		this.crawledDataSaver = crawlerSaveDao;
		return this;
	}

	public CrawlerConfig<T> setValidator(Validator<T> validator)
	{
		this.validator = validator;
		return this;
	}

	public CrawlerConfig<T> setUrlsToStart(List<String> newUrls)
	{
		urlsToStart.clear();
		if (newUrls == null || newUrls.isEmpty())
		{
			throw new IllegalArgumentException("We need at least one url to start. Or we are going to be a new search engine?");
		}
		urlsToStart.addAll(newUrls);
		return this;
	}

	public CrawlerConfig<T> setPatternTimeouts(List<PatternTimeout> patterTimeouts)
	{
		this.patterTimeouts.clear();
		if (patterTimeouts == null || patterTimeouts.isEmpty())
		{
			return this;
		}
		this.patterTimeouts.addAll(patterTimeouts);
		return this;
	}

	public CrawlerConfig<T> setCrawlers(int crawlers)
	{
		if (crawlers < 1)
		{
			throw new IllegalArgumentException("Amount of crawlers can not be less than 1. How do you think will do all work?");
		}
		this.crawlers = crawlers;
		return this;
	}

	public int getRestartWebClientEveryNPages()
	{
		return restartWebClientEveryNPages;
	}

	public CrawlerConfig<T> setRestartWebClientEveryNPages(int restartWebClientEveryNPages)
	{
		this.restartWebClientEveryNPages = restartWebClientEveryNPages;
		return this;
	}
}
