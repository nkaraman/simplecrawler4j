package simple.crawler4j;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.sun.istack.internal.NotNull;

import simple.crawler4j.url.WebURL;

public class PageResults<T>
{
	private HtmlPage page;
	private T results;
	private final Set<WebURL> urls = new HashSet<>();

	public PageResults(@NotNull HtmlPage page, @NotNull T results)
	{
		this.page = page;
		this.results = results;
	}

	public HtmlPage getPage()
	{
		return page;
	}

	public T getResults()
	{
		return results;
	}

	public Set<WebURL> getUrls()
	{
		return urls;
	}

	public void setUrls(Collection<WebURL> pageUrls)
	{
		urls.clear();
		if (pageUrls != null)
		{
			urls.addAll(pageUrls);
		}
	}
}
