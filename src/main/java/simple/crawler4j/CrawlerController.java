package simple.crawler4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.eclipse.jetty.util.ConcurrentHashSet;

import com.sun.istack.internal.NotNull;

import simple.crawler4j.url.WebURL;

public class CrawlerController<T>
{
	private final Set<WebURL> visitedPages = new ConcurrentHashSet<>();
	private final Queue<WebURL> needToVisitQueue = new ConcurrentLinkedQueue<>();
	private final List<Crawler<T>> crawlers = new ArrayList<>();
	private final CrawlerConfig<T> config;
	private final ExecutorService executor;

	public CrawlerController(@NotNull CrawlerConfig<T> config, @NotNull CrawlerFactory<T> factory)
	{
		this.config = config;
		this.executor = Executors.newFixedThreadPool(config.getCrawlers());
		for (int i = 0; i < config.getCrawlers(); i++)
		{
			crawlers.add(factory.createInstance(config));
		}
	}

	public void start(long expectedTimeWorkInMinutes) throws InterruptedException
	{
		if (config.getCrawledDataSaver() != null)
		{
			needToVisitQueue.addAll(config.getCrawledDataSaver().preloadUrls().stream().map(this::toWebUrl).collect(Collectors.toList()));
		}
		needToVisitQueue.addAll(config.getUrlsToStart().stream().map(this::toWebUrl).collect(Collectors.toList()));
		crawlers.stream().map(c -> new CrawlerThread(c, needToVisitQueue, config, visitedPages)).forEach(c -> executor.execute(c));
		executor.shutdown();
		executor.awaitTermination(expectedTimeWorkInMinutes, TimeUnit.MINUTES);
	}

	private WebURL toWebUrl(String stringUrl)
	{
		WebURL webURL = new WebURL(stringUrl, null);
		webURL.setDepth((short) 0);
		return webURL;
	}

}
