package simple.crawler4j.url;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

/**
 * Created by mkaraman on 2/27/17.
 */
public class WebURLTest
{
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	private static final String basicDomain = "helloworld.com";
	private static final String wwwWithDomain = "www." + basicDomain;

	@Test
	public void getDomainTest()
	{
		WebURL url = new WebURL("http://" + wwwWithDomain + "/sample.php?some=value&another=value#test");
		assertEquals(basicDomain, url.getDomain());
	}

	@Test
	public void getDomain2Test()
	{
		WebURL url = new WebURL(wwwWithDomain);
		assertEquals(basicDomain, url.getDomain());
	}

	@Test
	public void getDomain3Test()
	{
		WebURL url = new WebURL(basicDomain);
		assertEquals(basicDomain, url.getDomain());
	}

	@Test
	public void getDomain4Test()
	{
		expectedException.expect(IllegalArgumentException.class);
		WebURL url = new WebURL(null);
	}

	@Test
	public void getUrl () {
		WebURL url = new WebURL(basicDomain);
		Assert.assertEquals("http://" + basicDomain, url.getURL());
	}

}