package simple.crawler4j;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import simple.crawler4j.url.WebURL;

/**
 * Created by mkaraman on 2/27/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class CrawlerThreadTest
{
	@Mock
	Crawler<Object> crawler;
	CrawlerConfig<Object> emptyCrawlerConfigs = new CrawlerConfig<>();
	Queue<WebURL> emptyQueue = new ArrayDeque<>();

	@Before
	public void init () {
		emptyCrawlerConfigs.setTimeoutMilliseconds(1);
	}

	@Test
	public void shouldStopWithoutWork() throws Exception
	{
		Set<WebURL> visitedPages = new HashSet<>();
		CrawlerThread ct = new CrawlerThread(crawler, emptyQueue, emptyCrawlerConfigs, visitedPages);
		Thread t = new Thread(ct);
		t.start();
		TimeUnit.SECONDS.sleep(1L);
		if (t.isAlive()) {
			Assert.fail("The thread should be already stopped working");
		}
	}

	@Test
	public void shouldAddVisitedUrlsAndCallCrawlerToProcess() throws Exception
	{
		Set<WebURL> visitedPages = new HashSet<>();
		Queue<WebURL> needToVisitQueue = new ArrayDeque<>();
		String url = "http://helloworld.com/";
		WebURL webURL = new WebURL(url);
		needToVisitQueue.add(webURL);

		CrawlerThread ct = new CrawlerThread(crawler, needToVisitQueue, emptyCrawlerConfigs, visitedPages);
		ct.run();
		Assert.assertFalse(visitedPages.isEmpty());
		Assert.assertEquals(1, visitedPages.size());
		Assert.assertTrue(visitedPages.contains(webURL));
		Mockito.verify(crawler, Mockito.times(1)).fetchAndProcess(url);
	}
}