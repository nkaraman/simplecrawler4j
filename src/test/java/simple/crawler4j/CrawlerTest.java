package simple.crawler4j;

import java.io.IOException;
import java.util.Collections;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import simple.crawler4j.url.WebURL;

/**
 * Created by mkaraman on 2/27/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class CrawlerTest
{

	public static final String RANDOM_URL = "http://helloworld.com";
	@Mock
	public WebClientFactory webClientFactory;
	@Mock
	public WebClient webClient;
	@Mock
	public HtmlPage htmlPage;

	CrawlerConfig emptyConfig = new CrawlerConfig();

	@Before
	public void init() throws IOException
	{
		Mockito.when(webClientFactory.create(Mockito.any())).thenReturn(webClient);
		Mockito.when(webClient.getPage(Mockito.anyString())).thenReturn(htmlPage);
	}

	@Test
	public void shouldGoThrowWholeWorkflow() throws Exception
	{
		ResultToCheck resultToCheck = new ResultToCheck();
		Crawler crawler = generateCrawler(resultToCheck);

		crawler.fetchAndProcess(RANDOM_URL);

		Assert.assertTrue("OnAuthenticationCalled is not called", resultToCheck.isOnAuthenticationCalled);
		Assert.assertTrue("OnNextPagesSearchCalled is not called", resultToCheck.isOnNextPagesSearchCalled);
		Assert.assertTrue("OnWebClientConfigCalled is not called", resultToCheck.isOnWebClientConfigCalled);
		Assert.assertTrue("OnSaveCalled is not called", resultToCheck.isOnSaveCalled);
		Assert.assertTrue("OnVisitCalled is not called", resultToCheck.isOnVisitCalled);

		Mockito.verify(webClient, Mockito.times(1)).getPage(RANDOM_URL);
	}

	private Crawler generateCrawler(final ResultToCheck resultToCheck)
	{
		return new Crawler(emptyConfig, webClientFactory)
		{
			@Override
			public boolean shouldBeVisited(String url)
			{
				return true;
			}

			@Override
			public Object onVisit(HtmlPage page)
			{
				resultToCheck.isOnVisitCalled = true;
				return resultToCheck.resultObject;
			}

			@Override
			public void onWebClientConfig(WebClient webClient)
			{
				resultToCheck.isOnWebClientConfigCalled = true;
			}

			@Override
			public void onAuthentication(WebClient webClient)
			{
				resultToCheck.isOnAuthenticationCalled = true;
			}

			@Override
			public Set<WebURL> onNextPagesSearch(HtmlPage page, String context)
			{
				resultToCheck.isOnNextPagesSearchCalled = true;
				return Collections.emptySet();
			}

			@Override
			public void onSave(PageResults pageResults)
			{
				resultToCheck.isOnSaveCalled = true;
			}
		};
	}

	class ResultToCheck
	{
		Object resultObject = new Object();
		boolean isOnAuthenticationCalled = false;
		boolean isOnNextPagesSearchCalled = false;
		boolean isOnWebClientConfigCalled = false;
		boolean isOnSaveCalled = false;
		boolean isOnVisitCalled = false;
	}
}